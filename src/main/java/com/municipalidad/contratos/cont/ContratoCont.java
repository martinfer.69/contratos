/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.municipalidad.contratos.cont;

import com.municipalidad.contratos.adapter.ContratoAdapter;
import com.municipalidad.contratos.entities.ContratoType;
import com.municipalidad.contratos.entities.Contrato;
import com.municipalidad.contratos.entities.Secretaria;
import com.municipalidad.contratos.util.NumeroLetras;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author martin
 */
public class ContratoCont {
    
    private Map<Integer, Secretaria> secretarias;
    
    public ContratoCont() {
        secretarias = new HashMap<>();
        secretarias.put(1,new Secretaria(1, "INTENDENCIA", "el Intendente" ,"ING. LEONARDO ALBERTO STELATTO"));
        secretarias.put(2,new Secretaria(2, "SECRETARIA DE GOBIERNO", "el Secretario", "DR. JUAN PABLO RAMIREZ"));
        secretarias.put(3,new Secretaria(3, "SECRETARIA DE HACIENDA", "el Secretario", "C.P. SEBASTIAN ALEJANDRO GUASTAVINO"));
        secretarias.put(4,new Secretaria(4, "SECRETARIA DE SALUD, MEDIO AMBIENTE Y DESARROLLO HUMANO", "la Secretaria", "DRA. LHEA BEATRIZ ALEGRE"));
        secretarias.put(5,new Secretaria(5, "SECRETARIA DE OBRAS Y SERVICIOS PUBLICOS", "el Secretario", "ING. DANIEL ALEJANDRO VIGO"));
        secretarias.put(10,new Secretaria(10, "SECRETARIA DE DESARROLLO ECONOMICO", "el Secretario", "MG. CLAUDIO ARIEL AGUILAR"));
        secretarias.put(13,new Secretaria(13, "SECRETARIA DE CULTURA Y TURISMO", "el Secretario", "Sr. BENITO ALMIRO DEL PUERTO"));
        secretarias.put(14,new Secretaria(14, "SECRETARIA DE MOVILIDAD URBANA", "el Secretario", "ING. LUCAS MARTIN JARDIN"));
        secretarias.put(16,new Secretaria(16, "UNIDAD DE COORDINACION Y CONTROL DE GESTION", "la Coordinadora", "Tec. MARIA YOLANDA ASUNCION"));
        secretarias.put(17,new Secretaria(17, "SECRETARIA DE PLANIFICACION, ESTRATEGICA Y TERRITORIAL", "el Secretario", "ARQ. LUIS DIEGO PAREDES"));
    }
   
    public List<Contrato> transform(List<List> list, ContratoAdapter adapter, ContratoType type){
        List<Contrato> retorno = new LinkedList<Contrato>();
        for (List l : list) {
            if (list.indexOf(l) > 0){
                Contrato contrato = new Contrato();
                contrato.setApellidoNombre((String) l.get(adapter.getApellidoNombre()));
                if (ContratoType.CON_RELACION.equals(type)){
                    contrato.setCategoria((String) l.get(adapter.getCategoria()));
                }
                Secretaria secretaria = secretarias.get(Integer.parseInt((String) l.get(adapter.getDependencia())));
                contrato.setDependencia(secretaria.getNombre());
                contrato.setResponsable(secretaria.getResponsable());
                contrato.setCargo(secretaria.getCargo());
                contrato.setDocumento((String) l.get(adapter.getDocumento()));
                contrato.setDuracionMeses(Integer.parseInt((String) l.get(adapter.getDuracionMeses())));
                contrato.setFechaDesde((Date) l.get(adapter.getFechaDesde()));
                contrato.setFechaHasta((Date) l.get(adapter.getFechaHasta()));
                if (!ContratoType.CON_RELACION.equals(type)){
                    contrato.setMontoMensual(Float.parseFloat(((String)l.get(adapter.getMontoMensual())).replaceAll(",", ".")));
                    contrato.setMontoTotal(contrato.getMontoMensual() * 3);
                }
                retorno.add(contrato);
            }
        }
        return retorno;
    }
    
    public JasperPrint generateReport(Contrato contrato, ContratoType type) throws JRException {
        NumeroLetras numeroLetras = new NumeroLetras();
        String jasperfile = "SinRelacion.jasper";
        String titulo = "CONTRATO DE LOCACION DE SERVICIOS";
        if (ContratoType.CON_RELACION.equals(type)){
            jasperfile = "ConRelacion.jasper";
        }
        JasperReport jr = (JasperReport) JRLoader.loadObject(this.getClass().getClassLoader().getResource(jasperfile));
        Map parametros = new HashMap<String, Object>();
        Locale esLocale = new Locale("es", "ES");//para trabajar en español
        DecimalFormat decimalFormat = new DecimalFormat("#,###.00");
        DecimalFormat simpleDecimalFormat = new DecimalFormat("#.00");
        SimpleDateFormat formatter = new SimpleDateFormat("dd 'de' MMMM 'de' YYYY", esLocale);
        parametros.put("apellidoNombre", contrato.getApellidoNombre());
        parametros.put("documento", contrato.getDocumento());
        String duracionMeses = String.format("%s (%s)",contrato.getDuracionMeses().toString(),numeroLetras.convertir(contrato.getDuracionMeses().toString(), true, "",""));
        parametros.put("duracionMeses", duracionMeses);
        parametros.put("fechaDesde", formatter.format(contrato.getFechaDesde()).toUpperCase());
        parametros.put("fechaHasta", formatter.format(contrato.getFechaHasta()).toUpperCase());
        if (contrato.getMontoTotal() != null){
            String montoTotal = String.format("$ %s (%s)",decimalFormat.format(contrato.getMontoTotal()), numeroLetras.convertir(simpleDecimalFormat.format(contrato.getMontoTotal()), true, "PESOS","CENTAVOS"));
            parametros.put("montoTotal", montoTotal);
        }
        if (contrato.getMontoMensual() != null){
            String montoMensual = String.format("$ %s (%s)",decimalFormat.format(contrato.getMontoMensual()), numeroLetras.convertir(simpleDecimalFormat.format(contrato.getMontoMensual()), true, "PESOS","CENTAVOS"));
            parametros.put("montoMensual", montoMensual);
        }
        parametros.put("dependencia", contrato.getDependencia().toUpperCase());
        parametros.put("cargo", contrato.getCargo().toUpperCase());
        parametros.put("responsable", contrato.getResponsable().toUpperCase());
        parametros.put("categoria", contrato.getCategoria());
        parametros.put("ordenanza", "Ordenanza V N° 41 Decreto 1132/2019");
        parametros.put("titulo", titulo);

        JREmptyDataSource ds = new JREmptyDataSource();
        JasperPrint jp = JasperFillManager.fillReport(jr, parametros, ds);
        //byte[] exportReportToPdf = JasperExportManager.exportReportToPdf(jp);
        return jp;
    }


}
