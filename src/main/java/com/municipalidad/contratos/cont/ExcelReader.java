/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.municipalidad.contratos.cont;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.apache.poi.hssf.extractor.ExcelExtractor;
import org.apache.poi.hssf.usermodel.HSSFCell;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.apache.poi.ss.usermodel.DateUtil;

/**
 *
 * @author martin
 */
public class ExcelReader {
  
        /**
     * Read from excel 
     * @param file
     * @return Contrato
     * @throws IOException 
     */
    public static List<List> readFromExcel(String file) throws IOException, Exception {
        switch (file.substring(file.lastIndexOf(".") + 1, file.length())) {
            case "xls":
                return readXLS(file);
            case "xlsx":
                return readXLSX(file);
            default:
                throw new Exception(String.format("El archivo %s no es un excel valido", file));
        }
    }
    
    public static List<List> readXLSX(String file) throws IOException {
        XSSFWorkbook book = new XSSFWorkbook(new FileInputStream(file));
        XSSFSheet sheet = book.getSheetAt(0);
        DataFormatter fmt = new DataFormatter();
        
        List<List> retorno = new LinkedList<>();
        for (Iterator rowIterator = sheet.rowIterator(); rowIterator.hasNext();) {
            XSSFRow row = (XSSFRow) rowIterator.next();
            LinkedList<Object> rowList = new LinkedList<Object>();
            for (Iterator celIterator = row.cellIterator(); celIterator.hasNext();) {
                XSSFCell cell = (XSSFCell) celIterator.next();
                Object value = null;
                switch (cell.getCellType()){
                        case XSSFCell.CELL_TYPE_STRING:
                            value = cell.getStringCellValue();
                            break;
                        case XSSFCell.CELL_TYPE_NUMERIC:
                            if (DateUtil.isCellDateFormatted(cell)){
                                value = cell.getDateCellValue();
                            }else{
                                value = fmt.formatCellValue(cell);
                            }
                            break;    
                        default:
                            break;
                }
                rowList.add(value);
            }
            retorno.add(rowList);
        }
        book.close();
        return retorno;
    }
    
    public static List<List> readXLS(String file) throws IOException {
        HSSFWorkbook book = new HSSFWorkbook(new FileInputStream(file));
        HSSFSheet sheet = book.getSheetAt(0);
        DataFormatter fmt = new DataFormatter();
        
        List<List> retorno = new LinkedList<>();
        for (Iterator rowIterator = sheet.rowIterator(); rowIterator.hasNext();) {
            HSSFRow row = (HSSFRow) rowIterator.next();
            LinkedList<Object> rowList = new LinkedList<Object>();
            for (Iterator celIterator = row.cellIterator(); celIterator.hasNext();) {
                HSSFCell cell = (HSSFCell) celIterator.next();
                Object value = null;
                switch (cell.getCellType()){
                        case XSSFCell.CELL_TYPE_STRING:
                            value = cell.getStringCellValue();
                            break;
                        case XSSFCell.CELL_TYPE_NUMERIC:
                            if (DateUtil.isCellDateFormatted(cell)){
                                value = cell.getDateCellValue();
                            }else{
                                value = fmt.formatCellValue(cell);
                            }
                            break;    
                        default:
                            break;
                }
                rowList.add(value);
            }
            retorno.add(rowList);
        }
        book.close();
        return retorno;
    }
    
}
