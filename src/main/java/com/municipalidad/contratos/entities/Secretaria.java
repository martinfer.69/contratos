/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.municipalidad.contratos.entities;

/**
 *
 * @author martin
 */
public class Secretaria {
    
    private Integer id;
    private String nombre;
    private String cargo;
    private String responsable;

    public Secretaria() {
    }

    public Secretaria(Integer id, String nombre, String cargo, String responsable) {
        this.id = id;
        this.cargo = cargo;
        this.nombre = nombre;
        this.responsable = responsable;
    }

    public String getCargo() {
        return cargo;
    }

    public void setCargo(String cargo) {
        this.cargo = cargo;
    }
    
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getResponsable() {
        return responsable;
    }

    public void setResponsable(String responsable) {
        this.responsable = responsable;
    }
    
}
