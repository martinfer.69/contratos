/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.municipalidad.contratos.entities;

/**
 *
 * @author martin
 */
public enum ContratoType {
    SIN_RELACION, CON_RELACION, TRANSITO;
}
