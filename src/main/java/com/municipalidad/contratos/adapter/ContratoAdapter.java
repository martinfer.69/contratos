/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.municipalidad.contratos.adapter;

/**
 *
 * @author martin
 */
public class ContratoAdapter {
    
    private Integer apellidoNombre;
    private Integer documento;
    private Integer duracionMeses;
    private Integer fechaDesde;
    private Integer fechaHasta;
    private Integer montoTotal;
    private Integer montoMensual;
    private Integer dependencia;
    private Integer categoria;

    public ContratoAdapter() {
    }

    public Integer getApellidoNombre() {
        return apellidoNombre;
    }

    public void setApellidoNombre(Integer apellidoNombre) {
        this.apellidoNombre = apellidoNombre;
    }

    public Integer getDocumento() {
        return documento;
    }

    public void setDocumento(Integer documento) {
        this.documento = documento;
    }

    public Integer getDuracionMeses() {
        return duracionMeses;
    }

    public void setDuracionMeses(Integer duracionMeses) {
        this.duracionMeses = duracionMeses;
    }

    public Integer getFechaDesde() {
        return fechaDesde;
    }

    public void setFechaDesde(Integer fechaDesde) {
        this.fechaDesde = fechaDesde;
    }

    public Integer getFechaHasta() {
        return fechaHasta;
    }

    public void setFechaHasta(Integer fechaHasta) {
        this.fechaHasta = fechaHasta;
    }

    public Integer getMontoTotal() {
        return montoTotal;
    }

    public void setMontoTotal(Integer montoTotal) {
        this.montoTotal = montoTotal;
    }

    public Integer getMontoMensual() {
        return montoMensual;
    }

    public void setMontoMensual(Integer montoMensual) {
        this.montoMensual = montoMensual;
    }

    public Integer getDependencia() {
        return dependencia;
    }

    public void setDependencia(Integer dependencia) {
        this.dependencia = dependencia;
    }

    public Integer getCategoria() {
        return categoria;
    }

    public void setCategoria(Integer categoria) {
        this.categoria = categoria;
    }
    
    
}
